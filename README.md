# Springer build info

This is a sbt plugin that makes it easy to add build information to your project and its artifacts.

## Usage

Simply add the following two lines to `project/plugins.sbt`:

    addSbtPlugin("com.springer" % "sbt-build-info" % "0.x") // replace 0.x with latest version
    
    resolvers += "Springer Repository" at "http://repo.tools.springer-sbm.com:8081/nexus/content/repositories/releases/"

This will add a new sbt task called `buildInfo`:

    > sbt
    > buildInfo
    > [info] Created: target/build.info
    
### Usage from another task

Here's an example of how you can use `buildInfo` from another task:

    import sbt._
    import com.springer.sbt.buildinfo.Keys.buildInfo

    private def zipTask = { (buildInfo)
      map { (info) =>
          val otherFiles = ...
          val buildInfoFile = (info) pair flat
          IO.zip(otherFiles ++ buildInfoFile, file("foo.zip"))
      }
    }

## Latest Version

You'll find the latest version of the library [in our nexus repo](http://repo.tools.springer-sbm.com:8081/nexus/content/repositories/releases/com/springer/).

## Build

To build from source, there is a script in the project root that loads the correct sbt version to build and package the project:

    ./build

## Release Notes

### 0.1

Initial release

## Sample build.info

    # -----------------------------------------------------------------------------
    #  _           _ _     _     _        __
    # | |__  _   _(_) | __| |   (_)_ __  / _| ___
    # | '_ \| | | | | |/ _` |   | | '_ \| |_ / _ \
    # | |_) | |_| | | | (_| |   | | | | |  _| (_) |
    # |_.__/ \__,_|_|_|\__,_|   |_|_| |_|_|  \___/
    #
    # Generated on Tue Jul 08 16:06:28 BST 2014
    # by patric using lacnbefo062.springer-sbm.com/10.0.14.35
    # -----------------------------------------------------------------------------
    ---
    name: content-api
    version: LOCAL
    timestamp: 1404831988309
    revision: 471ce9f6dd7ffa7fadc4f4b0e45294b12a287f94
    branch: master
    recent-commits:
      - "471ce9f pat: changing modules file name."
      - "0f33c87 pat: using generic ml-deploy script to deploy modules."
      - 8c8fb42 README.md edited online with Bitbucket
      - 6c9ea34 README.md edited online with Bitbucket
      - "f471880 pat: using 127.0.0.1 because 0.0.0.0 conflicts with boot2docker config."
      - "a07101c Rob: update username for xquery deploy"
      - "f287be1 pat: added twitter repo for unresolved dependencies."
      - "b233f09 pat: zipkin spike."
      - "4136d4d Rob: moved bash smoke test spike to own repo - bash-http-tester"
      - 10522a5 exit if one arg not supplied
    dependencies:
      - scala-compiler-2.10.3.jar
      - scala-library-2.10.3.jar
      - jetty-all-9.1.4.v20140401.jar
      - javax.websocket-api-1.0.jar
      - javax.servlet-api-3.1.0.jar
      - javax.servlet-3.0.0.v201112011016.jar
      - slf4j-api-1.7.5.jar
      - slf4j-api-1.7.5-sources.jar
      - logback-gelf-0.10p1.jar
      - logback-access-1.1.0.jar
      - logback-core-1.1.0.jar
      - gson-2.1.jar
      - json4s-native_2.10-3.2.4.jar
      - json4s-core_2.10-3.2.4.jar
      - json4s-ast_2.10-3.2.4.jar
      - paranamer-2.5.2.jar
      - scalap-2.10.0.jar
      - scala-reflect-2.10.3.jar
      - scalatra-swagger_2.10-2.2.2.jar
      - scalatra_2.10-2.2.2.jar
      - scalatra-common_2.10-2.2.2.jar
      - grizzled-slf4j_2.10-1.0.1.jar
      - rl_2.10-0.4.4.jar
      - juniversalchardet-1.0.3.jar
      - mime-util-2.1.3.jar
      - joda-time-2.2.jar
      - joda-convert-1.2.jar
      - akka-actor_2.10-2.1.2.jar
      - config-1.0.0.jar
      - scalatra-json_2.10-2.2.2.jar
      - json4s-ext_2.10-3.2.4.jar
      - swagger-core_2.10.0-1.2.0.jar
      - jackson-jaxrs-json-provider-2.0.0.jar
      - jackson-module-jaxb-annotations-2.0.0.jar
      - jsr311-api-1.1.1.jar
      - swagger-annotations_2.10.0-1.2.0.jar
      - metrics-core_2.10-0.112.jar
      - logback-classic-1.1.0-sources.jar
      - logback-classic-1.1.0.jar
      - metrics-core-3.0.1.jar
      - metrics-graphite-3.0.1.jar
      - metrics-jvm-3.0.1.jar
      - metrics-jetty9_2.10-0.112.jar
      - jetty-webapp-9.1.4.v20140401.jar
      - jetty-xml-9.1.4.v20140401.jar
      - jetty-util-9.1.4.v20140401.jar
      - jetty-servlet-9.1.4.v20140401.jar
      - jetty-security-9.1.4.v20140401.jar
      - jetty-server-9.1.4.v20140401.jar
      - jetty-http-9.1.4.v20140401.jar
      - jetty-io-9.1.4.v20140401.jar
      - metrics-jetty9-3.0.1.jar
      - http_2.10-0.55.jar
      - httpclient-4.2.2.jar
      - httpcore-4.2.2.jar
      - commons-logging-1.1.1.jar
      - commons-codec-1.6.jar
      - commons-codec-1.6-sources.jar
      - parboiled-core-1.1.4.jar
      - parboiled-scala_2.10-1.1.4.jar
      - scala-uri_2.10-0.3.6.jar
      - metrics-core-2.2.0.jar
      - commons-configuration-1.8.jar
      - commons-lang-2.6.jar
      - archaius-core-0.4.1.jar
      - rxjava-core-0.14.11.jar
      - hystrix-core-1.3.7.jar
      - properties_2.10-0.2.jar
      - commons-io-2.1.jar
      - metrics-httpclient-2.2.0.jar
      - metrics-graphite-2.2.0.jar
      - tracing-jetty_2.10-0.6.jar
      - tracing-core_2.10-0.6.jar
      - config_2.10-0.9.jar
      - finatra_2.10-1.5.3.jar
      - finatra_2.10-1.5.3-sources.jar
      - twitter-server_2.10-1.6.1.jar
      - finagle-core_2.10-6.13.1.jar
      - netty-3.8.1.Final.jar
      - util-app_2.10-6.13.2.jar
      - util-core_2.10-6.13.2.jar
      - util-collection_2.10-6.13.2.jar
      - jsr305-2.0.1.jar
      - javax.inject-1.jar
      - guava-16.0.jar
      - commons-collections-3.2.1.jar
      - util-hashing_2.10-6.13.2.jar
      - util-jvm_2.10-6.13.2.jar
      - util-logging_2.10-6.13.2.jar
      - finagle-http_2.10-6.13.1.jar
      - util-codec_2.10-6.13.2.jar
      - finagle-zipkin_2.10-6.13.1.jar
      - finagle-thrift_2.10-6.13.1.jar
      - libthrift-0.5.0.jar
      - scrooge-core_2.10-3.12.3.jar
      - jackson-core-2.2.2.jar
      - jackson-databind-2.2.2.jar
      - jackson-annotations-2.2.2.jar
      - jackson-module-scala_2.10-2.2.2.jar
      - finagle-stats_2.10-6.13.1.jar
      - metrics-0.0.9.jar
      - base-0.0.67.jar
      - quantity-0.0.52.jar
      - collections-0.0.55.jar
      - util-system-mocks-0.0.50.jar
      - stats-util-0.0.30.jar
      - util-0.0.75.jar
      - util-executor-service-shutdown-0.0.36.jar
      - jdk-logging-0.0.32.jar
      - stats-0.0.74.jar
      - stat-registry-0.0.17.jar
      - stat-0.0.19.jar
      - stats-provider-0.0.44.jar
      - application-action-0.0.54.jar
      - net-util-0.0.59.jar
      - guice-3.0.jar
      - aopalliance-1.0.jar
      - cglib-2.2.1-v20090111.jar
      - asm-3.1.jar
      - util-sampler-0.0.40.jar
      - jsr166e-1.0.0.jar
      - scalatest_2.10-1.9.2.jar
      - compiler-0.8.14.jar
      - tracing-http_2.10-0.6.jar
      - tokeniser_2.10-0.1.jar
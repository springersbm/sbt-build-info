package com.springer.sbt.buildinfo

import sbt._
import sbt.Keys._
import java.io.{File, PrintWriter, FileWriter}
import java.util.Date
import java.net.InetAddress
import java.lang.System

object Keys {
  val buildInfo = TaskKey[File]("build-info", "Creates a build.info file.")
}

object BuildInfoPlugin extends AutoPlugin {

  import Keys._

  override lazy val projectSettings: Seq[Setting[_]] = Seq(buildInfo <<= buildInfoTask)
  
	override def requires: Plugins = empty
  
	override def trigger: PluginTrigger = allRequirements

  private val RecentCommitsMaxCount = 10

  private def buildInfoTask = {
    (target, name, version, fullClasspath in Runtime, streams) map {
      (target, name, version, classpath, streams) =>

        val info = Seq(
          "name" -> name,
          "version" -> version,
          "timestamp" -> timestamp(),
          "revision" -> revision(),
          "branch" -> branch(),
          "recent-commits" -> recentCommits(),
          "build-url" -> buildUrl(),
          "dependencies" -> selectJars(classpath.files)
        )

        val file = new File(target, "build.info")

        write(info, header(), file)
        streams.log.info("Created: " + file)
        file
    }
  }

  private def header() = {
    """|# -----------------------------------------------------------------------------
       |#  _           _ _     _     _        __
       |# | |__  _   _(_) | __| |   (_)_ __  / _| ___
       |# | '_ \| | | | | |/ _` |   | | '_ \| |_ / _ \
       |# | |_) | |_| | | | (_| |   | | | | |  _| (_) |
       |# |_.__/ \__,_|_|_|\__,_|   |_|_| |_|_|  \___/
       |#
       |# Generated on %s
       |# by %s using %s
       |# -----------------------------------------------------------------------------
       |---""".stripMargin.format(new Date(), env("USER").getOrElse(""), InetAddress.getLocalHost)
  }

  private def timestamp(): Long = System.currentTimeMillis()

  private def revision(): String = ("git rev-parse HEAD" !!).trim

  private def branch(): String = ("git rev-parse --abbrev-ref HEAD" !! ).trim

  private def recentCommits(maxCount: Int = RecentCommitsMaxCount): Seq[String] = {
    ((s"git log --oneline --max-count=$maxCount") !!).split("\n")
  }

  private def buildUrl(): Option[URL] =
    (env("GO_SERVER_URL"), env("GO_PIPELINE_NAME"), env("GO_PIPELINE_COUNTER"), env("GO_STAGE_NAME"), env("GO_STAGE_COUNTER")) match {
      case (Some(url), Some(pipeline), Some(counter), Some(stage), Some(run)) => Some(new URL(s"${url}pipelines/$pipeline/$counter/$stage/$run"))
      case _ => None
    }

  private def selectJars(files: Seq[File]) = {
    files.filter(_.ext == "jar").map(_.name)
  }

  private def env(name: String): Option[String] = Option(System.getenv(name))

  private def write(info: Seq[(String, Any)], header: String, file: File) {
    val writer = new PrintWriter(new FileWriter(file))

    writer.println(header)

    info foreach {
      _ match {
        case (key, values: Seq[_]) =>
          writer.println(s"$key:")
          values foreach { value => writer.println("  - " + escape(value)) }
        case (key, option: Option[_]) =>
          option.map(value => writer.println(s"$key: " + escape(value)))
        case (key, value: Any) =>
          writer.println(s"$key: " + escape(value))
      }
    }

    writer.close()
  }

  private def escape(value: Any): Any = {
    if (needsEscaping(value)) "\"" + escapeDoubleQuotes(value) + "\""
    else value
  }

  private def needsEscaping(value: Any): Boolean = String.valueOf(value).contains(":")

  private def escapeDoubleQuotes(value: Any): String = String.valueOf(value).replaceAll("\"", "\\\\\"")
}
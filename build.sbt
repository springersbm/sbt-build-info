sbtPlugin := true

name := "sbt-build-info"

organization := "com.springer"

version := Option(System.getenv("GO_PIPELINE_LABEL")).getOrElse("LOCAL")

publishTo := Some("Springer Nexus Repo" at "http://repo.tools.springer-sbm.com:8081/nexus/content/repositories/releases/")